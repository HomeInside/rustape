# rustape :vhs:

[Rust](https://www.rust-lang.org) is a modern systems programming language focusing on safety, speed, and concurrency. It accomplishes these goals by being memory safe without using garbage collection.

**Rustape** is a command line application that allows you to use [SQLite](https://sqlite.org/index.html) to manage a movie list database, illustrates various Rust concepts and standard libraries.

For more information, see the [Rust official documentation](https://www.rust-lang.org/learn) and [Rust by Example (RBE)](https://doc.rust-lang.org/rust-by-example/).


## Requirements

 - [rustup](https://rustup.rs) to install Rust :wink:
 - [Rust](https://www.rust-lang.org) **1.53.0** and above
 - [Cargo](https://doc.rust-lang.org/cargo/) **1.53.0** and above


## Recommended tools
 - [rustfmt](https://github.com/rust-lang/rustfmt) **1.4.37-stable** and above
 - [clippy](https://github.com/rust-lang/rust-clippy) **0.1.53** and above


## Getting Started

### Clone the repo and make it yours:

```bash
$ git clone git@gitlab.com:HomeInside/rustape.git
```

## Tutorials

 - We are working on it... :smile:


## License

The full text of the licenses can be found in the MIT-LICENSE' file.
