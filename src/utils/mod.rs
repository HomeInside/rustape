use std::env;
use std::env::VarError;
use std::fs;

pub mod user_input {
    use std::io::{self, Write};

    pub fn get_input_int(prompt: &str) -> u32 {
        loop {
            let mut line = String::new();

            // Reads the input from STDIN and places it in the String named line.
            print!("{}", prompt);
            let _ = io::stdout().flush().expect("Failed to flush stdout.");

            io::stdin().read_line(&mut line).expect("Failed to read input.");

            let input_trimmed = line.trim();
            // Convert to another type.
            // If successful, bind to a new u32 variable named input.
            // If failed, restart the loop.
            let input = match input_trimmed.parse::<u32>() {
                Ok(input_to_int) => input_to_int,
                Err(_) => {
                    println!("opcion invalida: debe ser un numero mayor a 0 (cero)");
                    continue;
                }
            };

            return input;
        }
    }

    pub fn get_input_default<U: std::str::FromStr>(prompt: &str) -> U {
        loop {
            let mut line = String::new();

            // Reads the input from STDIN and places it in the String named line.
            print!("{}", prompt);
            let _ = io::stdout().flush().expect("Failed to flush stdout.");

            io::stdin().read_line(&mut line).expect("Failed to read input.");

            let input_trimmed = line.trim();
            // Convert to another type.
            // If successful, bind to a new variable named input.
            // If failed, restart the loop.
            let input = match input_trimmed.parse::<U>() {
                Ok(parsed_input) => parsed_input,
                Err(_) => continue,
            };

            return input;
        }
    }
}

pub fn clear_screen() {
    //std::process::Command::new("clear").status().unwrap();
    print!("\x1B[2J \x1B[0;0f");
    //print!("{}[2J", 27 as char);
    //print!("\x1B[2J");
}

pub fn get_home_path() -> Result<String, VarError> {
    let db_path = "HOME";

    env::var(db_path)

    /*match env::var(db_path) {
        Ok(val) => Ok(val),
        Err(e) => Err(e),
    }*/
}

pub fn create_dir(dir: &str) -> std::io::Result<&str> {
    fs::create_dir_all(dir)?;
    Ok(dir)
}
