pub mod database {
    use crate::utils;
    use rusqlite::Error;
    use rusqlite::{params, Connection, OpenFlags, Result};
    use std::fmt;

    const RUSTAPE_WORK_DIR: &str = ".rustape";
    const RUSTAPE_DB: &str = "rustape.db";

    #[derive(Debug)]
    pub struct Movies {
        id: Option<i32>,
        name: String,
        year: i32,
    }

    // Similarly, implement `Display` for `Movies`
    impl fmt::Display for Movies {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            //write!(f, "id: {}, name: {}, year: {}", self.id, self.name, self.year)
            write!(f, "name: {}", self.name)
        }
    }

    fn init_db(conn: &Connection) {
        println!("creating db...");

        let sql = "CREATE TABLE IF NOT EXISTS movies
        (
            id   INTEGER not null primary key autoincrement,
            name TEXT    not null,
            year INTEGER not null
        );";

        conn.execute(sql, params![]).expect("init_db: Can not execute query");
    }

    pub fn connect() -> Result<Connection> {
        let home_path = utils::get_home_path().unwrap();

        let work_dir = format!("{}/{}", home_path, RUSTAPE_WORK_DIR);

        match utils::create_dir(&work_dir) {
            Ok(dir) => {
                println!("working directory in: {}", dir);
            }
            Err(e) => {
                println!("working directory Error: {:?}", e.to_string());
            }
        }

        let db_path = format!("{}/{}", work_dir, RUSTAPE_DB);

        let conn = Connection::open_with_flags(
            &db_path,
            OpenFlags::SQLITE_OPEN_READ_WRITE | OpenFlags::SQLITE_OPEN_CREATE,
        );

        if conn.is_ok() {
            println!("connecting to: {}... is ok", db_path);
            init_db(conn.as_ref().unwrap());
        } else {
            println!("an error occurred while connecting to the DB: {:?}", db_path);
            println!("{:?}", conn.as_ref().unwrap());
        }

        conn
    }

    pub fn all_movies(conn: &Connection) -> Result<Vec<String>, Error> {
        let mut stmt = conn.prepare("SELECT * FROM movies;")?;

        let movies_list = stmt.query_map([], |row| {
            Ok(Movies {
                id: row.get(0)?,
                name: row.get(1)?,
                year: row.get(2)?,
            })
        })?;

        let mut result: Vec<String> = Vec::new();

        for movie in movies_list {
            //result.push(movie.unwrap());
            let x = movie.unwrap();
            let id = x.id;
            let name = x.name;
            let year = x.year;
            let item = format!("{}. {} ({})", &id.unwrap(), &name, &year);
            result.push(item);
        }
        Ok(result)
    }

    pub fn search_movie(conn: &Connection, id: i32) -> Result<Vec<String>, Error> {
        let mut stmt = conn.prepare("SELECT * FROM movies WHERE id = :id")?;

        let rows = stmt.query_map(&[(":id", &id)], |row| {
            Ok(Movies {
                id: row.get(0)?,
                name: row.get(1)?,
                year: row.get(2)?,
            })
        })?;

        let mut result = Vec::new();

        for row in rows {
            //result.push(name_result?);
            let x = row.unwrap();
            let id = x.id;
            let name = x.name;
            let year = x.year;
            let item = format!("{}. {} ({})", &id.unwrap(), &name, &year);
            result.push(item);
        }
        Ok(result)
    }

    pub fn search_one_movie(conn: &Connection, name: String) -> Result<Vec<String>, Error> {
        let mut stmt = conn.prepare("SELECT * FROM movies WHERE name LIKE :name")?;
        let sql = format!("%{}%", name);

        let rows = stmt.query_map(&[(":name", &sql)], |row| {
            Ok(Movies {
                id: row.get(0)?,
                name: row.get(1)?,
                year: row.get(2)?,
            })
        })?;

        let mut result = Vec::new();

        for row in rows {
            //result.push(name_result?);
            let x = row.unwrap();
            let id = x.id;
            let name = x.name;
            let year = x.year;
            let item = format!("{}. {} ({})", &id.unwrap(), &name, &year);
            result.push(item);
        }
        Ok(result)
    }

    pub fn delete_movie(conn: &Connection, id: i32) -> Result<usize> {
        let mut stmt = conn.prepare("DELETE FROM movies WHERE id = :id")?;
        let last_id = stmt.execute(&[(":id", &id)])?;
        Ok(last_id)
    }

    pub fn add_movie(conn: &Connection, name: String, year: i32) -> Result<usize> {
        let new_movie = Movies { id: None, name, year };

        let mut stmt = conn.prepare("INSERT INTO movies (name, year) VALUES(:name, :year)")?;
        stmt.execute(params![new_movie.name, new_movie.year])?;

        let last_row_id = conn.last_insert_rowid() as usize;

        Ok(last_row_id)
    }
}
