mod menu;
mod movies;
mod utils;

use movies::database as db;
use utils::user_input::*;

extern crate term_table;
use term_table::Table;

fn main() {
    let table = Table::new();
    utils::clear_screen();
    menu::main_menu(table);
    let conn = db::connect();

    loop {
        let table = Table::new();
        let menu_option: u32 = get_input_int("Menu>_ ");

        match menu_option {
            // menu
            1 => {
                utils::clear_screen();
                menu::main_menu(table);
            }
            // listado
            2 => {
                utils::clear_screen();
                let result = db::all_movies(conn.as_ref().unwrap());
                if result.is_ok() {
                    //println!("result all_movies success: {:?}", result);
                    menu::list_menu(table, result);
                } else {
                    println!("result all_movies error: {:?}", result);
                }
            }
            // Buscar por ID
            3 => {
                utils::clear_screen();
                println!("Buscar por ID");
                println!("ingresa el ID de una pelicula y presiona [ENTER]");
                let movie_id: i32 = get_input_default("movieID>_ ");
                let result = db::search_movie(conn.as_ref().unwrap(), movie_id);
                if result.is_ok() {
                    //println!("result search_movie success: {:?}", result);
                    menu::list_menu(table, result);
                } else {
                    println!("result search_movie error: {:?}", result);
                }
            }
            // Buscar por nombre
            4 => {
                utils::clear_screen();
                println!("Buscar por nombre");
                println!("ingresa el nombre (parcial o completo) de una pelicula y presiona [ENTER]");
                let movie_name: String = get_input_default("movieID>_ ");
                let result = db::search_one_movie(conn.as_ref().unwrap(), movie_name);
                if result.is_ok() {
                    //println!("result search_movie success: {:?}", result);
                    menu::list_menu(table, result);
                } else {
                    println!("result search_one_movie error: {:?}", result);
                }
            }
            // Nuevo
            5 => {
                utils::clear_screen();
                println!("nueva pelicula...");

                println!("ingresa el nombre de una pelicula y presiona [ENTER]");
                let movie_name: String = get_input_default("Name>_ ");

                println!("ingresa el año de la pelicula y presiona [ENTER]");
                let movie_year: i32 = get_input_default("Year>_ ");

                let result = db::add_movie(conn.as_ref().unwrap(), movie_name, movie_year);
                if result.is_ok() {
                    //println!("result add_movie success: {:?}", result);
                    menu::result_menu(table, result, 5);
                } else {
                    println!("result add_movie error: {:?}", result);
                }
            }
            // Eliminar
            6 => {
                utils::clear_screen();
                println!("Eliminar pelicula");
                println!("ingresa el ID de una pelicula y presiona [ENTER] para eliminarla");
                let movie_id: i32 = get_input_default("movieID>_ ");
                let result = db::delete_movie(conn.as_ref().unwrap(), movie_id);
                if result.is_ok() {
                    //println!("result delete_movie success: {:?}", result);
                    menu::result_menu(table, result, 6);
                } else {
                    println!("result delete_movie error: {:?}", result);
                }
            }
            // Acerca de...
            7 => {
                utils::clear_screen();
                menu::about(table);
            }
            //Salir
            8 => {
                println!("saliendo...");
                break;
            }
            // Handle the rest of cases
            _ => {
                utils::clear_screen();
                println!("opcion no valida, regresando al menu.");
                menu::main_menu(table);
            }
        }
    }
}
