extern crate term_table;
use term_table::{
    row::Row,
    table_cell::{Alignment, TableCell},
};

use rusqlite::Error;

use term_table::{Table, TableStyle};
const VERSION: &str = env!("CARGO_PKG_VERSION");

fn custom_style() -> TableStyle {
    TableStyle {
        top_left_corner: '╔',
        top_right_corner: '╗',
        bottom_left_corner: '╚',
        bottom_right_corner: '╝',
        outer_left_vertical: '╠',
        outer_right_vertical: '╣',
        outer_bottom_horizontal: '╩',
        outer_top_horizontal: '╦',
        intersection: '╬',
        vertical: '║',
        horizontal: '═',
    }
}

pub fn main_menu(mut table: Table) {
    table.max_column_width = 80;

    table.style = TableStyle::elegant();

    table.add_row(Row::new(vec![
        TableCell::new_with_alignment("Welcome to movie db in Rust", 4, Alignment::Center),
        TableCell::new_with_alignment(format!("Version: {}", VERSION), 1, Alignment::Right),
    ]));

    table.add_row(Row::new(vec![TableCell::new_with_alignment(
        "ingresa una opcion [1/9] y luego navega por el menu",
        5,
        Alignment::Right,
    )]));

    table.add_row(Row::new(vec![TableCell::new_with_col_span("1. Menu", 5)]));

    table.add_row(Row::new(vec![TableCell::new_with_col_span("2. Listado", 5)]));

    table.add_row(Row::new(vec![TableCell::new_with_col_span("3. Buscar por ID", 5)]));

    table.add_row(Row::new(vec![TableCell::new_with_col_span("4. Buscar por nombre", 5)]));
    table.add_row(Row::new(vec![TableCell::new_with_col_span("5. Nuevo", 5)]));
    table.add_row(Row::new(vec![TableCell::new_with_col_span("6. Eliminar", 5)]));

    table.add_row(Row::new(vec![TableCell::new_with_col_span("7. Acerca de...", 5)]));

    table.add_row(Row::new(vec![TableCell::new_with_col_span("8. Salir", 5)]));

    println!("{}", table.render());
}

pub fn list_menu(mut table: Table, movies_list: Result<Vec<String>, Error>) {
    let mut items = movies_list.into_iter();

    loop {
        match items.next() {
            Some(movie) => {
                table.max_column_width = 80;
                table.style = TableStyle::elegant();

                table.add_row(Row::new(vec![
                    TableCell::new_with_alignment("Welcome to movie db in Rust", 4, Alignment::Center),
                    TableCell::new_with_alignment(format!("Version: {}", VERSION), 1, Alignment::Right),
                ]));

                table.add_row(Row::new(vec![TableCell::new_with_alignment(
                    "listado de peliculas",
                    5,
                    Alignment::Right,
                )]));

                if !movie.is_empty() {
                    for item in movie.iter() {
                        table.add_row(Row::new(vec![TableCell::new_with_col_span(item, 5)]));
                    }
                } else {
                    table.add_row(Row::new(vec![TableCell::new_with_col_span(
                        "no existe una pelicula con ese ID",
                        5,
                    )]));
                }

                println!("{}", table.render());
            }
            None => break,
        }
    }
}

pub fn result_menu(mut table: Table, result: Result<usize, Error>, status: usize) {
    let id = result.unwrap();

    table.max_column_width = 80;
    table.style = TableStyle::elegant();

    table.add_row(Row::new(vec![
        TableCell::new_with_alignment("Welcome to movie db in Rust", 4, Alignment::Center),
        TableCell::new_with_alignment(format!("Version: {}", VERSION), 1, Alignment::Right),
    ]));

    table.add_row(Row::new(vec![TableCell::new_with_alignment(
        "Resultado de operaciones",
        5,
        Alignment::Right,
    )]));
    match status {
        5 => {
            let message: &str;
            if id > 0 {
                message = "La Pelicula fue agregada con exito";
            } else {
                message = "La pelicula no pudo ser agregada a la DB";
            }

            table.add_row(Row::new(vec![TableCell::new_with_col_span(message, 5)]));
        }
        6 => {
            let message: &str;
            if id > 0 {
                message = "La Pelicula eliminada con exito";
            } else {
                message = "La pelicula no existe o ya fue eliminada";
            }

            table.add_row(Row::new(vec![TableCell::new_with_col_span(message, 5)]));
        }
        _ => {
            table.add_row(Row::new(vec![TableCell::new_with_col_span(
                format!("Ocurrio un error: ({})", status),
                5,
            )]));
        }
    }
    println!("{}", table.render());
}

pub fn about(mut table: Table) {
    const PKG_NAME: &str = env!("CARGO_PKG_NAME");
    const AUTHORS: &str = env!("CARGO_PKG_AUTHORS");

    table.max_column_width = 80;

    table.style = custom_style();

    table.add_row(Row::new(vec![
        TableCell::new_with_alignment("Welcome to movie db in Rust", 4, Alignment::Center),
        TableCell::new_with_alignment(format!("Version: {}", VERSION), 1, Alignment::Right),
    ]));

    table.add_row(Row::new(vec![TableCell::new_with_alignment(
        "Made with Rust",
        5,
        Alignment::Right,
    )]));

    table.add_row(Row::new(vec![TableCell::new_with_alignment(
        format!("App name: {}", PKG_NAME),
        5,
        Alignment::Right,
    )]));
    table.add_row(Row::new(vec![TableCell::new_with_alignment(
        format!("Author: {}", AUTHORS),
        5,
        Alignment::Right,
    )]));

    println!("{}", table.render());
}
